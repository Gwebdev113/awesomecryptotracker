export const mainThemes = {
  bgColor: '#212025',
  bgColor2: '#17171a;',
  bgColor3: '#1b1b1f',
  borderBottomColor: '#222531',
  positiveGraphGradientColor2: '#28a745',
  positiveGraphGradientColor1: '#8ceb13',
  negativeGraphGradientColor2: '#dc3545',
  negativeGraphGradientColor1: '#ff0000',
};

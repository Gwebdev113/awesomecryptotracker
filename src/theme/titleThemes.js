export const titleTheme = {
  bigTitle: {
    mainText: '#ffffff',
    weight: 'bold',
    size: '25px',
    margin: '10px'
  },
  mediumTitle: {
    mainText: '#ffffff',
    weight: 'bold',
    size: '18px',
    margin: '10px'
  },
};

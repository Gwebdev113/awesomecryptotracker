export const itemThemes = {
  coinTheme: {
    mainText: '#ffffff',
    titleFontWeight: 'bold',
    shortTitle: '#D3D3D3',
    priceFontSize: '14px',
    positiveColor: '#28a745',
    negativeColor: '#dc3545',
    percentageSize: '12px',
  },
};

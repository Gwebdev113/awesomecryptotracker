import * as shape from 'd3-shape';
import {scaleLinear} from 'd3-scale';
import {coinGraphHeight, coinGraphWidth} from './constants';
//import {mixPath, useVector, serialize, parse} from 'react-native-redash';

const SIZE = coinGraphHeight;
const WIDTH = coinGraphWidth;

export const parsedValue = sparkLineData => {
  const priceList = sparkLineData.prices;
  const formattedValues = priceList.map(price => [
    parseFloat(price[1]),
    price[0],
  ]);
  const prices = formattedValues.map(value => value[0]);
  const dates = formattedValues.map(value => value[1]);
  const scaleX = scaleLinear()
    .domain([Math.min(...dates), Math.max(...dates)])
    .range([0, WIDTH]);
  const minPrice = Math.min(...prices);
  const maxPrice = Math.max(...prices);
  const scaleY = scaleLinear().domain([minPrice, maxPrice]).range([SIZE, 0]);

  const finalParsedValue = shape
    .line()
    .x(([, x]) => scaleX(x))
    .y(([y]) => scaleY(y))
    .curve(shape.curveBasis)(formattedValues);

  return finalParsedValue;
};

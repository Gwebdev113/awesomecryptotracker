import {Dimensions} from 'react-native';

export const coinGraphWidth = 180;
export const coinGraphHeight = 100;

export const deviceWidth = Dimensions.get('window').width;
export const deviceHeight = Dimensions.get('window').height;

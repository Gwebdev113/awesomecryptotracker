import React from 'react';
import {ScrollView, View} from 'react-native';
import { MainTabbar } from '../../components/tabbar/MainTabbar';
import {CoinList} from './components/CoinList/CoinList';
import {CoinMainTitle} from './components/CoinMainTitle/CoinMainTitle';
import {Header} from './components/Header/Header';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScrollView>
        <Header />
        <CoinMainTitle />
        <CoinList />
      </ScrollView>
    );
  }
}

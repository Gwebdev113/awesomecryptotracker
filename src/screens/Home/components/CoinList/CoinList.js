import React, {useState, useEffect} from 'react';
import {Container, Loader} from './styled';
import {ActivityIndicator, FlatList, ScrollView} from 'react-native';
import {CoinInfoItem} from '../../../../components/listItems/CoinInfo';

export const CoinList = () => {
  const [coinData, setCoinData] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [cryptoPage, setCryptoPage] = useState(1);

  function getCoinGeckoData() {
    return fetch(
      `https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=50&page=${cryptoPage}&sparkline=false`,
    )
      .then(response => response.json())
      .then(data => {
        setCoinData({...data});
        setLoading(false);
      });
  }

  useEffect(() => {
    console.log('call data');
    getCoinGeckoData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cryptoPage]);

  const renderItem = ({item}) => (
    <CoinInfoItem
      name={item.name}
      shortName={item.symbol.toUpperCase()}
      coinIcon={item.image}
      currentPrice={item.current_price}
      percentagePriceChange={item.price_change_percentage_24h}
      marketCap={item.market_cap}
      coinId={item.id}
      rank={item.market_cap_rank}
      volume={item.total_volume}
      supply={item.circulating_supply}
    />
  );

  return (
    <Container>
      <ScrollView>
        <FlatList
          data={coinData && Object.values(coinData)}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
        {isLoading === true && <Loader color={'#ffffff'} size={50} />}
      </ScrollView>
    </Container>
  );
};

import React from 'react';
import {Container, AppLogo} from './styled';
import {MainTitle} from '../../../../components/titles/MainTitleText';
import {titleTheme} from '../../../../theme/titleThemes';

export const Header = ({title, theme}) => {
  return (
    <Container>
      <AppLogo source={require('../../../../assets/logos/app-logo.webp')} />
      <MainTitle title={'CryptoTracker'} theme={titleTheme.bigTitle} />
    </Container>
  );
};

import styled from 'styled-components/native';
import { mainThemes } from '../../../../theme/mainThemes';

export const Container = styled.View`
  background: ${mainThemes.bgColor2};
  padding: 10px 15px;
  border-bottom-color: ${mainThemes.borderBottomColor};
  border-bottom-width: 1px;
  flex-direction: row;
  align-items: center;
`;


export const AppLogo = styled.Image`
  height: 40px;
  width: 40px;
`
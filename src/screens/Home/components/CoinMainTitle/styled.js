import styled from 'styled-components/native';
import { mainThemes } from '../../../../theme/mainThemes';

export const Container = styled.View`
  background: ${mainThemes.bgColor3};
  padding: 20px 10px;
`;

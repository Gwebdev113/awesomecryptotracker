import React from 'react';
import {Container} from './styled';
import {MainTitle} from '../../../../components/titles/MainTitleText';
import {titleTheme} from '../../../../theme/titleThemes';

export const CoinMainTitle = () => {
  return (
    <Container>
      <MainTitle
        title={'The Best Cryptocurrencies of the market'}
        theme={titleTheme.mediumTitle}
      />
    </Container>
  );
};

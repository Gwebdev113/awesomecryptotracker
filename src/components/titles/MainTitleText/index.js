import React from 'react';
import {Title} from './styled';
import {ThemeProvider} from 'styled-components';

export const MainTitle = ({title, theme}) => (
  <ThemeProvider theme={theme}>
    <Title>{title}</Title>
  </ThemeProvider>
);

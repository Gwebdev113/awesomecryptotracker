import styled from 'styled-components/native';

export const Title = styled.Text`
  color: ${props => props.theme.mainText};
  font-weight: ${props => props.theme.weight};
  font-size: ${props => props.theme.size};
  margin: ${props => props.theme.margin};
`;

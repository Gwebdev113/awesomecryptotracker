import React, {useEffect, useState} from 'react';
import Svg, {Path} from 'react-native-svg';
import * as shape from 'd3-shape';
import {deviceHeight, deviceWidth} from '../../../utils/constants';
import {SafeAreaView, View, Animated, StyleSheet} from 'react-native';
import {MainTabbarContainer} from './styled';
import {StaticTabbar} from './StaticTabbar';
import {mainThemes} from '../../../theme/mainThemes';

const tabList = [
  {
    name: 'list',
    screen: 'Home',
  },
  {
    name: 'star',
    screen: 'Favorites',
  },
  {
    name: 'graph-pie',
    screen: 'Portfolio',
  },

  {
    name: 'torso',
    screen: 'User',
  },
];
const tabItemWidth = deviceWidth / tabList.length;
const tabItemHeight = 70;
const AnimatedSvg = Animated.createAnimatedComponent(Svg);

const left = shape
  .line()
  .x(d => d.x)
  .y(d => d.y)([
  {x: 0, y: 0},
  {x: deviceWidth, y: 0},
]); //Create line chart - straight line

const tabItem = shape //Array of icons
  .line()
  .x(d => d.x)
  .y(d => d.y)
  .curve(shape.curveBasis)([
  {x: deviceWidth, y: 0},
  {x: deviceWidth + 5, y: 0},
  {x: deviceWidth + 20, y: 10}, //margin
  {x: deviceWidth + 20, y: tabItemHeight},
  {x: deviceWidth + tabItemWidth - 20, y: tabItemHeight - 0},
  {x: deviceWidth + tabItemWidth - 20, y: 10},
  {x: deviceWidth + tabItemWidth - 5, y: 0},
  {x: deviceWidth + tabItemWidth, y: 0},
]);

const right = shape
  .line()
  .x(d => d.x)
  .y(d => d.y)([
  {x: deviceWidth + tabItemWidth, y: 0},
  {x: deviceWidth * 2, y: 0},
  {x: deviceWidth * 2, y: deviceHeight},
  {x: 0, y: deviceHeight},
  {x: 0, y: 0},
]);

const d = `${left} ${tabItem} ${right}`;

export const MainTabbar = ({state, navigation}) => {
  const currentRoute = state.history[state.history.length - 1].key;

  const animationValue =
    -deviceWidth +
    tabItemWidth * tabList.findIndex(tab => currentRoute.includes(tab.screen));
  const value = new Animated.Value(animationValue);

  console.log(animationValue);

  return (
    <>
      <MainTabbarContainer height={tabItemHeight} width={deviceWidth}>
        <AnimatedSvg
          {...{height: tabItemHeight, width: deviceWidth * 2}}
          style={{transform: [{translateX: value}]}}>
          <Path {...{d}} fill={mainThemes.bgColor3} />
        </AnimatedSvg>
        <View style={StyleSheet.absoluteFill}>
          <StaticTabbar
            tabList={tabList}
            tabHeight={tabItemHeight}
            tabWidth={tabItemWidth}
            animationValue={value}
            currentRoute={state.routeNames[0]}
            navigate={navigation}
          />
        </View>
      </MainTabbarContainer>
    </>
  );
};

import styled from 'styled-components/native';
import {mainThemes} from '../../../theme/mainThemes';

export const MainTabbarContainer = styled.View`
  flex-direction: row;
  height: ${props => props.height};
  width: ${props => props.width};
`;

export const IconList = styled.View`
  flex-direction: row;
`;

export const IconContainer = styled.View`
  flex: 1;
  height: ${props => props.tabHeight};
  justify-content: center;
  align-items: center;
`;

export const ActiveIcon = styled.View`
  position: absolute;
  width: ${props => props.tabWidth};
  left: ${props => props.tabLeft};
  height: ${props => props.tabHeight};
  justify-content: center;
  align-items: center;
`;

export const ActiveIconContainer = styled.View`
  background-color: ${mainThemes.bgColor2};
  width: 45px;
  height: 45px;
  align-items: center;
  justify-content: center;
  border-radius: 100;
  margin-bottom: 10px;
`;

import React, {useEffect, useState} from 'react';
import {TouchableWithoutFeedback, Animated} from 'react-native';
import Icon from 'react-native-vector-icons/dist/Foundation';
import {deviceWidth} from '../../../utils/constants';
import {
  IconList,
  IconContainer,
  ActiveIcon,
  ActiveIconContainer,
} from './styled';

const AnimatedIconContainer = Animated.createAnimatedComponent(IconContainer);

export const StaticTabbar = ({
  tabList,
  tabHeight,
  tabWidth,
  animationValue,
  currentRoute,
  navigate,
}) => {
  const onPress = (index, route) => {
    console.log(-deviceWidth + tabWidth * index)
    Animated.spring(animationValue, {
      toValue: -deviceWidth + tabWidth * index,
      useNativeDriver: true,
    }).start(() => navigate.navigate(route));
  };

  return (
    <IconList>
      {tabList.map((tab, index) => {
        const opacityChange = animationValue.interpolate({
          inputRange: [
            -deviceWidth + tabWidth * (index - 1),
            -deviceWidth + tabWidth * index,
            -deviceWidth + tabWidth * (index + 1),
          ],
          outputRange: [1, 0, 1],
          extrapolate: 'clamp',
        });

        return (
          <>
            <TouchableWithoutFeedback
              key={index}
              onPress={() => onPress(index, tab.screen)}>
              <AnimatedIconContainer
                tabHeight={tabHeight}
                style={{opacity: opacityChange}}>
                <Icon size={30} name={tab.name} color="#ffffff" />
              </AnimatedIconContainer>
            </TouchableWithoutFeedback>
            <ActiveIcon
              tabWidth={tabWidth}
              tabLeft={tabWidth * index}
              tabHeight={tabHeight}>
              <ActiveIconContainer>
                <TouchableWithoutFeedback
                  key={index}
                  onPress={() => onPress(index, tab.screen)}>
                  <Icon size={30} name={tab.name} color="#ffffff" />
                </TouchableWithoutFeedback>
              </ActiveIconContainer>
            </ActiveIcon>
          </>
        );
      })}
    </IconList>
  );
};

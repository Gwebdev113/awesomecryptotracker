import styled from 'styled-components/native';
import {ScrollView} from 'react-native';
import {mainThemes} from '../../../theme/mainThemes';

export const PressWrapper = styled.TouchableOpacity`
  flex-direction: row;
`;

export const ScrollWrapper = styled(ScrollView)``;

export const Container = styled.View`
  padding: 35px 20px;
  flex-direction: row;
  border-bottom-width: 1px;
  border-bottom-color: ${mainThemes.borderBottomColor};
  align-items: center;
`;

export const CoinLogo = styled.Image`
  height: 45px;
  width: 45px;
  margin-left: 20px;
`;

export const CoinTitle = styled.View`
  width: 70px;
  margin: 0px 0px 0px 20px;
`;

export const Title = styled.Text`
  color: ${props => props.theme.mainText};
  font-weight: ${props => props.theme.titleFontWeight};
`;

export const ShortTitle = styled.Text`
  color: ${props => props.theme.shortTitle};
`;

export const CoinPrice = styled(Title)`
  font-size: ${props => props.theme.priceFontSize};
  width: 100px;
`;

export const PercentageChange = styled.Text`
  color: ${props => props.theme.mainText};
  font-weight: ${props => props.theme.titleFontWeight};
  background: ${props => props.changeColor};
  font-size: ${props => props.theme.percentageSize};
  padding: 5px 5px;
  width: 80px;
  text-align: center;
  margin: 0px 15px;
  border-radius: 25px;
`;

export const MarketCap = styled.Text`
  color: ${props => props.theme.mainText};
  font-weight: bold;
  margin: 0px 20px;
`;

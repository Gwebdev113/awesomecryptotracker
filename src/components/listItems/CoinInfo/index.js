import React, {useEffect, useState} from 'react';
import {
  PressWrapper,
  Container,
  CoinLogo,
  ScrollWrapper,
  CoinTitle,
  Title,
  ShortTitle,
  CoinPrice,
  PercentageChange,
  MarketCap,
} from './styled';
import {ThemeProvider} from 'styled-components';
import {itemThemes} from '../../../theme/itemThemes';
import {GraphMaker} from '../../graphs/GraphMaker';
import {Text, Modal, ActivityIndicator} from 'react-native';

export const CoinInfoItem = ({
  name,
  shortName,
  coinIcon,
  currentPrice,
  percentagePriceChange,
  marketCap,
  coinId,
  rank,
  volume,
  supply,
}) => {
  const [graphData, setGraphData] = useState(null);
  const [showOptionModal, setShowOptionModal] = useState(false);

  function getCoinGeckoData() {
    return fetch(
      `https://api.coingecko.com/api/v3/coins/${coinId}/market_chart?vs_currency=usd&days=1`,
    )
      .then(response => {
        return response.json()
      })
      .then(data => {
        setGraphData({...data});
      })
      .catch(error => console.error(error.message))
  }

  useEffect(() => {
    let timer = setTimeout(() => getCoinGeckoData(), 500);

    return () => {
      clearTimeout(timer);
    };
  });

  const margin = percentagePriceChange > 0;

  return (
    <ScrollWrapper horizontal showsHorizontalScrollIndicator={false}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showOptionModal}
        onRequestClose={() => {
          setShowOptionModal(!showOptionModal);
        }}>
        <Text>THIS IS A MODAL</Text>
      </Modal>
      <PressWrapper
        onPress={() => console.log(name)}
        onLongPress={() => setShowOptionModal(true)}>
        <Container>
          <ThemeProvider theme={itemThemes.coinTheme}>
            <ShortTitle>{rank}</ShortTitle>
          </ThemeProvider>
          <CoinLogo source={{uri: coinIcon}} resizeMode={'contain'} />
          <CoinTitle>
            <ThemeProvider theme={itemThemes.coinTheme}>
              <Title>{name}</Title>
              <ShortTitle>{shortName}</ShortTitle>
              <CoinPrice>{`$ ${currentPrice}`}</CoinPrice>
            </ThemeProvider>
          </CoinTitle>
        </Container>
        {graphData ? (
          <GraphMaker sparkLineData={graphData && graphData} margin={margin} />
        ) : (
          <ActivityIndicator color={'#ffffff'} size={30} />
        )}
        <Container>
          <ThemeProvider theme={itemThemes.coinTheme}>
            <PercentageChange
              changeColor={
                margin
                  ? itemThemes.coinTheme.positiveColor
                  : itemThemes.coinTheme.negativeColor
              }>
              {percentagePriceChange.toFixed(3)}
            </PercentageChange>
          </ThemeProvider>
          <ThemeProvider theme={itemThemes.coinTheme}>
            <MarketCap>
              {`Market Cap:\n$ ${marketCap
                .toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`}
            </MarketCap>
            <MarketCap>
              {`Volume in 24hs:\n$ ${volume
                .toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`}
            </MarketCap>
            <MarketCap>
              {`Circulating Supply:\n ${supply
                .toString()
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')} ${shortName}`}
            </MarketCap>
          </ThemeProvider>
        </Container>
      </PressWrapper>
    </ScrollWrapper>
  );
};

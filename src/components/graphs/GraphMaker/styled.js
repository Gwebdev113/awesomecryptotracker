import styled from 'styled-components/native';
import {mainThemes} from '../../../theme/mainThemes';

export const Container = styled.View`
  padding: 10px 10px;
  border-bottom-width: 1px;
  border-bottom-color: ${mainThemes.borderBottomColor};
`;

import React from 'react';
import {Container} from './styled';
import Svg, {Path, Defs, Stop, LinearGradient} from 'react-native-svg';

import {parsedValue} from '../../../utils/buildGraphsCoords';
import {coinGraphHeight, coinGraphWidth} from '../../../utils/constants';
import {Dimensions} from 'react-native';
import {mainThemes} from '../../../theme/mainThemes';

export const GraphMaker = ({sparkLineData, margin}) => {
  const values = parsedValue(sparkLineData);

  return (
    <Container>
      <Svg width={coinGraphWidth} height={coinGraphHeight}>
        <Defs>
          <LinearGradient
            gradientTransform="rotate(-90)"
            id="gradient"
            x1="0"
            y1="0"
            x2="0"
            y2="1">
            <Stop
              offset="0"
              stopColor={
                margin
                  ? mainThemes.positiveGraphGradientColor2
                  : mainThemes.negativeGraphGradientColor2
              }
            />
            <Stop
              offset="1"
              stopColor={
                margin
                  ? mainThemes.positiveGraphGradientColor1
                  : mainThemes.negativeGraphGradientColor1
              }
            />
          </LinearGradient>
        </Defs>
        <Path d={values} stroke="url(#gradient)" strokeWidth={3} />
      </Svg>
    </Container>
  );
};

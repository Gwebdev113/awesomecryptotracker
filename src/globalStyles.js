import {StyleSheet} from 'react-native';
import { mainThemes } from './theme/mainThemes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: mainThemes.bgColor,
  },
});

module.exports = styles;

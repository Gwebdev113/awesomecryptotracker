//LET'S DANCE
import React from 'react';
import type {Node} from 'react';
import {SafeAreaView, StatusBar, LogBox, View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './src/screens/Home/Home';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {mainThemes} from './src/theme/mainThemes';
import {MainTabbar} from './src/components/tabbar/MainTabbar';

LogBox.ignoreLogs(['Reanimated 2']);

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const App: () => Node = () => {
  const AppMainTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: mainThemes.bgColor,
    },
  };

  const ExampleScreen = () => (
    <View>
      <Text>User</Text>
    </View>
  );
  const MainTab = () => (
    <Tab.Navigator
      tabBar={props => <MainTabbar {...props} />}
      initialRouteName={'Portfolio'}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Favorites" component={ExampleScreen} />
      <Tab.Screen name="Portfolio" component={ExampleScreen} />
      <Tab.Screen name="User" component={ExampleScreen} />
    </Tab.Navigator>
  );

  return (
    <SafeAreaProvider>
      <NavigationContainer theme={AppMainTheme}>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="Main" component={MainTab} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default App;
